#include <Arduino_DebugUtils.h>
#define WIFI 1
#include <EEPROM.h>
#include <WiFi.h>
#include <ESPmDNS.h>

// Set your network here:
const String ssid     = "open";
const String password = "";

// The board's name (in mDNS) is read from EEPROM.  But if no name is written into the EEPROM, then use this name
const char* DefaultBoardName = "myc3_b";

// What port should I listen on?
const int Port = 80;
// How many simultaneous connections are allowed?
const int MaxClients = 4;

// Set your service name if you want to auto-discover nodes here.  Pick any string.
const char* mDnsService = "esp32c3";

// Change this to configure what the minimum signal strength you are willing to connect to is
const int MinRssi = -75;

const int MIN_CNXN_FILL_INTERVAL = 1000;  // Because the connection logic takes time

#define LED_RED 3
#define LED_GREEN 4
#define LED_BLUE 5

#define LED_YELLOW 18
#define LED_WHITE 19

WiFiServer server(Port, MaxClients);
WiFiClient clients[MaxClients];


void DumpSysInfo() 
{
  esp_chip_info_t sysinfo;
  esp_chip_info(&sysinfo);
  Serial.print("ESP32 Model: ");
  Serial.print((int)sysinfo.model);
  Serial.print("; Features: 0x");
  Serial.print((int)sysinfo.features, HEX);
  Serial.print("; Cores: ");
  Serial.print((int)sysinfo.cores);
  Serial.print("; Revision: r");
  Serial.println((int)sysinfo.revision);
}

// Prints what WiFis I can hear.  Returns the strongest open network, unless the preferred is available and above the minimum.
// DISCONNECTS from WIFI!!!
String AnalyzeWiFis(String preferred = String())
{
    // Set WiFi to station mode and disconnect from an AP if it was previously connected
    WiFi.mode(WIFI_STA);
    WiFi.disconnect();
    delay(100);

    int n = WiFi.scanNetworks();
    Serial.println("scan done");
    if (n == 0) 
    {
        Serial.println("no networks found");
    } 
    else 
    {
        Serial.print(n);
        Serial.println(" networks found");
        int preferredIdx = -1;
        int maxRssiIdx = -1;
        for (int i = 0; i < n; ++i) 
        {
            if (WiFi.SSID(i) == preferred) preferredIdx = i;
            // Find the strongest open network
            if (WiFi.encryptionType(i) == WIFI_AUTH_OPEN)
                if (WiFi.RSSI(maxRssiIdx) < WiFi.RSSI(i))
                    maxRssiIdx = i;
            // Print SSID and RSSI for each network found
            Serial.print(i);
            Serial.print(": ");
            Serial.print(WiFi.SSID(i));
            Serial.print(" (");
            Serial.print(WiFi.RSSI(i));
            Serial.print(")");
            Serial.println((WiFi.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*");
        }

        // If our preferred is available and strong enough, return it.
        if ((preferredIdx >= 0) && WiFi.RSSI(preferredIdx) >= MinRssi) return preferred;
        // Otherwise return the strongest open connection
        if (maxRssiIdx != -1)
            return WiFi.SSID(maxRssiIdx);
    }
    return String();
}

uint16_t GetDeviceId()
{
#if defined(ARDUINO_ARCH_ESP32)
  return ESP.getEfuseMac();
#else
  return ESP.getChipId();
#endif
}


void AdvertiseServices(const char *MyName)
{
  if (MDNS.begin(MyName))
  {
    Serial.println(F("mDNS responder started"));
    Serial.print(F("I am: "));
    Serial.println(MyName);
 
    // Add service to MDNS-SD
    MDNS.addService(mDnsService, "tcp", Port);
  }
  else
  {
    while (1) 
    {
      Serial.println(F("Error setting up MDNS responder"));
      delay(1000);
    }
  }
}

int64_t micros64()
{
    static int64_t rolls = 0;
    static uint32_t lastmicros = 0;
    uint32_t n = micros();
    //uint32_t p = n;
    n += 0xFC000000UL;
    //Debug.print(DBG_INFO, "micros64 n:%lu -> %lu lastmicros: %lld", p, n, lastmicros);
    
    if (n < lastmicros) 
    {
      rolls += 0x100000000UL;
      // Debug.print(DBG_INFO, "Rolled 0x%llx", rolls);
    }
    lastmicros = n;
    int64_t now = n;
    return (now+rolls);
}

typedef struct
{
    unsigned int magic;
    char boardName[32];
} ConfigData;

ConfigData configData;

const void WriteEEPROM()
{
  configData.magic = 7227;
  unsigned char* ptr = (unsigned char*) &configData;
  for (int i = 0; i< sizeof(ConfigData); i++)
  {
    EEPROM.write(i, *(ptr+i));
  }
  EEPROM.commit();
}

const void ReadEEPROM()
{
  unsigned char* ptr = (unsigned char*) &configData;
  for (int i = 0; i< sizeof(ConfigData); i++)
  {
    *(ptr+i) = EEPROM.read(i);
  }
}

// Get the board name if the eeprom data is  valid, otherwise return a hard-coded default
const char* BoardName()
{
  if (configData.magic != 7227)
      return DefaultBoardName;
  else
      return configData.boardName;
}

void WiFiConnector()
{
    if (WiFi.status() != WL_CONNECTED)
    {
        String connectTo = AnalyzeWiFis(String(ssid));
    
        if (connectTo != "")
        {
            if ((connectTo == ssid)&&(password != ""))  WiFi.begin(ssid.c_str(), password.c_str());
            else WiFi.begin(connectTo.c_str());
          
            Serial.print("Connecting to ");
            Serial.println(connectTo);
            size_t tries = 0;
            while ((WiFi.status() != WL_CONNECTED) && (tries < 100))
            {
                digitalWrite(LED_YELLOW, tries&1);
                digitalWrite(LED_WHITE, !(tries&1));                
                delay(100);
                Serial.print(".");
                tries++;    
            }

            if (WiFi.status() == WL_CONNECTED)
            {
                Serial.println("");
                Serial.println("WiFi connected.");
                Serial.println("IP address: ");
                Serial.println(WiFi.localIP());
                AdvertiseServices(BoardName());
                server.setNoDelay(true);
                server.begin();
            }
            else
                Serial.println("WiFi cannot connect");
        }
    }
}

#define LED_RED 3
#define LED_GREEN 4
#define LED_BLUE 5

#define LED_YELLOW 18
#define LED_WHITE 19

void setup() 
{
    Serial.begin(115200);
    Debug.setDebugLevel(DBG_VERBOSE);
    Debug.timestampOn();
    Debug.setDebugOutputStream(&Serial);

    DumpSysInfo();

    EEPROM.begin(sizeof(ConfigData));
    ReadEEPROM();

    if (configData.magic != 7227)
    {
        strcpy(configData.boardName, DefaultBoardName);
        WriteEEPROM();
        Debug.print(DBG_INFO, "Overwrote EEPROM with defaults");
    }       

    // Set the LED pins to OUTPUT so we can drive them
    pinMode(LED_RED, OUTPUT);
    pinMode(LED_GREEN, OUTPUT);
    pinMode(LED_BLUE, OUTPUT);
  
    pinMode(LED_YELLOW, OUTPUT);
    pinMode(LED_WHITE, OUTPUT);
}

boolean fillNewClient()
{

  WiFiClient c = server.available();

  if (c)
  {
    for (int cidx=0; cidx < MaxClients; cidx++)
    {
      if (clients[cidx] && (clients[cidx].fd()==c.fd())) return false;  // Not something new 
    }
  
    for (int cidx=0; cidx < MaxClients; cidx++)
    {
      if (!clients[cidx]) 
      {
        IPAddress ip = c.remoteIP();
        Debug.print(DBG_INFO, "New client in slot %d fd %d coming from %d.%d.%d.%d", cidx, c.fd(), ip[0], ip[1], ip[2], ip[3] );
        clients[cidx] = c;
        clients[cidx].setNoDelay(true);
        return true;
      }
    }
  }
  return false;
}

unsigned char readWait(WiFiClient& client)
{
  uint32_t start = millis();
  while (!client.available())
  {
    if (!client.connected()) 
    {
      Debug.print(DBG_INFO, "Client disconnect in readWait()");
      return 0;
    }
    delayMicroseconds(10);
  }
  return client.read();
}

size_t readWait(WiFiClient& client, uint8_t* buf, size_t total)
{
  size_t got=0;
  while(got < total)
  {
    while ((!client.available()) && client.connected())
    {
      delayMicroseconds(10);
    }
    if (!client.connected()) 
    {
      Debug.print(DBG_INFO, "Client disconnect in readWait(...)");
      return 0;
    }
    int amtRead = client.read(buf+got, total-got);
    if (amtRead < 0)
    {
      Debug.print(DBG_INFO, "Read failure in readWait(...)");
      client.stop();
      return got;
    }
    got += amtRead;
  }
  return got;
}

unsigned long lastFill = 0;

bool red=0,grn=0,blu=0,yel=0,wht=0;
void handleHttpLine(String& line, WiFiClient& client)
{
  String response = "Unknown request";

  if (line.startsWith("GET /RED"))
  {
      red = !red;
      digitalWrite(LED_RED, red); 
      response = String("OK. Red is ");
      response.concat(String(red ? "on" : "off"));
  }
  else if (line.startsWith("GET /GRN"))
  {
      grn = !grn;
      digitalWrite(LED_GREEN, grn); 
      response = String("OK. Green is ");
      response.concat(String(grn ? "on" : "off"));
  }
  else if (line.startsWith("GET /BLU"))
  {
      blu = !blu;
      digitalWrite(LED_BLUE, blu); 
      response = String("OK. Blue is ");
      response.concat(String(blu ? "on" : "off"));
  }
  else if (line.startsWith("GET /WHT"))
  {
      wht = !wht;
      digitalWrite(LED_WHITE, wht); 
      response = String("OK. White is ");
      response.concat(String(wht ? "on" : "off"));
  }
  else if (line.startsWith("GET /YEL"))
  {
      yel = !yel;
      digitalWrite(LED_YELLOW, yel); 
      response = String("OK. Yellow is ");
      response.concat(String(yel ? "on" : "off"));
  }
  else if (line.startsWith("GET /H"))
  {
    String chop = line.substring(6);
    int v = chop.toInt();
    if (v<22)
    {
        pinMode(v, OUTPUT);
        digitalWrite(v,HIGH);
        response = String("Ok. Set ") + String(v) + String(" high");
    } 
  }
  else if (line.startsWith("GET /L"))
  {
    String chop = line.substring(6);
    int v = chop.toInt();
    if (v<22)
    {
        pinMode(v, OUTPUT);
        digitalWrite(v,LOW);
        response = String("Ok. Set ") + String(v) + String(" low");
    } 
  }
  else if (line.startsWith("GET /I"))
  {
    String chop = line.substring(6);
    int v = chop.toInt();
    if (v<22)
    {
        pinMode(v, INPUT);
        auto val = digitalRead(v);
        response = String("Ok. Read ") + String(v) + String(" as ") + String(val);
    } 
  }
  else if (line.startsWith("GET /A"))  // AnalogRead does not seem to work in the Arduino ESPC3 yet.
  {
    String chop = line.substring(6);
    int v = chop.toInt();
    if (v<22)
    {
        pinMode(v, INPUT);
        auto val = analogRead(v);
        response = String("Ok. Read ") + String(v) + String(" as ") + String(val);
    } 
  }  
  else if (line.startsWith("GET /"))
  {
      // the content of the HTTP response follows the header:
      response = "";
  }
  else if (line.startsWith("GET"))
  {
      // the content of the HTTP response follows the header:
      response = "Not understood<br/>";
  }

  response += "<br/>Click <a href=\"/RED\">RED</a>, <a href=\"/GRN\">GREEN</a>, <a href=\"/BLU\">BLUE</a> to toggle the color on the RGB LED.<br/>"
              "Click <a href=\"/WHT\">WHITE</a> <a href=\"/YEL\">YELLOW</a> to toggle these LEDs.  Note that the hardware causes white to be off if yellow is on.<br/>";

  if (response != "")
  {
                // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
                // and a content-type so the client knows what's coming, then a blank line:
                client.println("HTTP/1.1 200 OK");
                client.println("Content-type:text/html");
                client.println();
    
                // the content of the HTTP response follows the header:
                client.print(response.c_str());
                // The HTTP response ends with another blank line:
                client.println();
                // HTTP disconnects after every request
                client.stop();
  }
  
}

bool HttpHandler()
{
    int emptySlots = MaxClients;
    int somethingHappened=0;
  
    //Debug.print(DBG_INFO, "stats: numFills: %d, last fill: %llu avg fill clock ticks: %llu", fastLEDnumFills, fastLEDlastFillDuration, fastLEDfillDuration/fastLEDnumFills);
    for (int cidx=0; cidx < MaxClients; cidx++)
    {  
      Debug.print(DBG_INFO, "client Loop %d", cidx);
      if (clients[cidx] && clients[cidx].connected())  // Fill an empty slot with a new client
      {
        emptySlots--;
        WiFiClient& client = clients[cidx];
        String currentLine = "";                // make a String to hold incoming data from the client
        Debug.print(DBG_INFO, "client %d is alive, %s data", client.fd(), client.available() ? "has" : "no");
        while (client.available() && client.connected())                  // if there's bytes to read from the client,
        {
          somethingHappened++;
          char c = readWait(client);             // read a byte, then
          if ((currentLine.length()==0)&&((c<'A') || (c>'Z')))  // Binary protocol handling
          // Its the beginning of the line, and the line doesn't begin wtih GET, HEAD, POST, PUT, DELETE, CONNECT, OPTIONS, or TRACE
          {

          }
          else  // HTTP handling
          {
            Serial.write(c);                    // print it out the serial monitor
            if (c == '\n')                      // if the byte is a newline character
            {
              Debug.print(DBG_INFO, "responding");
              // if the current line is blank, you got two newline characters in a row.
              // that's the end of the client HTTP request, so send a response:
              if (currentLine.length() == 0) 
              {
                // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
                // and a content-type so the client knows what's coming, then a blank line:
                client.println("HTTP/1.1 200 OK");
                client.println("Content-type:text/html");
                client.println();
    
                // the content of the HTTP response follows the header:
                client.print("Click <a href=\"/RED\">RED</a>, <a href=\"/GRN\">GREEN</a>, <a href=\"/BLU\">BLUE</a> to toggle the color on the RGB LED.<br>");
                client.print("Click <a href=\"/WHT\">WHITE</a> <a href=\"/YEL\">YELLOW</a> to toggle these LEDs.<br>");
    
                // The HTTP response ends with another blank line:
                client.println();
                // break out of the while loop:
                //break;
              } 
              else    // if you got a newline, then clear currentLine:
              {
                handleHttpLine(currentLine, client);
                currentLine = "";
              }
            } 
          
            if (c != '\r')    // if you got anything else but a carriage return character,
            {
              currentLine += c;      // add it to the end of the currentLine
            }
          }
        }
  
      if (!client.connected())
      {
        // close the connection:
        //client.stop();   
        Debug.print(DBG_INFO, "Client %d idx %d Disconnected.", client.fd(), cidx);
      }
    }
  
  }

  unsigned long now = millis();
  if ((somethingHappened==0)&&(now > lastFill + MIN_CNXN_FILL_INTERVAL))
  {
      lastFill = now;
      if (fillNewClient()) somethingHappened++;
  }
  
  return emptySlots != MaxClients;
}



void loop()
{
  WiFiConnector();

  size_t looping = 0;

  do
  {
      bool connected = HttpHandler();
      if (connected) looping = 600;  // Wait a minute for further commands; otherwise fall back into cycling LEDs as coded below this loop
      else delay(100);
      if (looping > 0) looping --;
  } while (looping > 0);
  
  int r = random(8);
  // Illuminate the RGB LED
  digitalWrite(LED_RED, (r & 1) > 0);     // Turn the LED on based on a particular bit in the random number
  digitalWrite(LED_GREEN, (r & 2) > 0);   
  digitalWrite(LED_BLUE, (r & 4) > 0);
  delay(500);                            // wait

  digitalWrite(LED_RED, (r & 1) == 0);    // Now change the LED to the inverted color
  digitalWrite(LED_GREEN, (r & 2) == 0);  
  digitalWrite(LED_BLUE, (r & 4) == 0); 
  delay(500);                       

  // Now turn on the 2 small indicator LEDs on the board
  digitalWrite(LED_YELLOW, 1);
  delay(200);     
  digitalWrite(LED_YELLOW, 0);
  digitalWrite(LED_WHITE, 1);
  delay(200);     
  digitalWrite(LED_WHITE, 0);
  digitalWrite(LED_RED, 0);    // Now change the LED to the inverted color
  digitalWrite(LED_GREEN, 0);  
  digitalWrite(LED_BLUE, 0); 
}
