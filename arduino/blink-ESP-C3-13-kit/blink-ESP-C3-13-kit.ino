/* Andrew Stone, 2021, Public Domain
  ESP32C3-13-kit blink leds

  Troubleshooting:
  Upload error: "A fatal error occurred: Timed out waiting for packet header"
  Answer: Baud rate MUST BE 460800 (tools->upload speed) (or --baud 460800 if running esptool.py on the command line)
*/

#define LED_RED 3
#define LED_GREEN 4
#define LED_BLUE 5

#define LED_YELLOW 18
#define LED_WHITE 19


void setup() 
{
  // Set the LED pins to OUTPUT so we can drive them
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);

  pinMode(LED_YELLOW, OUTPUT);
  pinMode(LED_WHITE, OUTPUT);
}


void loop() 
{
  int r = random(8);
  // Illuminate the RGB LED
  digitalWrite(LED_RED, (r & 1) > 0);     // Turn the LED on based on a particular bit in the random number
  digitalWrite(LED_GREEN, (r & 2) > 0);   
  digitalWrite(LED_BLUE, (r & 4) > 0);
  delay(1000);                            // wait

  digitalWrite(LED_RED, (r & 1) == 0);    // Now change the LED to the inverted color
  digitalWrite(LED_GREEN, (r & 2) == 0);  
  digitalWrite(LED_BLUE, (r & 4) == 0); 
  delay(1000);                       

  // Now turn on the 2 small indicator LEDs on the board
  digitalWrite(LED_YELLOW, 1);
  delay(500);     
  digitalWrite(LED_YELLOW, 0);
  digitalWrite(LED_WHITE, 1);
  delay(500);     
  digitalWrite(LED_WHITE, 0);
}
