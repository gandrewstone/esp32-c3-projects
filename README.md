# ESP32-C3 Resources and Projects

## Get your board up

See the arduino/blink-ESP-C3-13-kit directory.

## Networking

See the arduino/network-ESP-C3-12-kit directory.

## Reference Docs

[13u-kit board](esp-c3-13_specification.pdf)
[datasheet](esp32-c3_datasheet_en.pdf)
[chip reference manual](esp32-c3_technical_reference_manual_en.pdf)
